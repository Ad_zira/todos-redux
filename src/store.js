import { configureStore } from "@reduxjs/toolkit";
import todosReducer from "./features/todos/todosSlice";
import filtersReducer from "./features/filters/filtersSlice";

// const store = createStore(rootReducer, undefined, composedEnhancer)
// Pass enhancer as the second arg, since there's no preloadedState
const store = configureStore({
  reducer: {
    todos: todosReducer,
    filters: filtersReducer
  }
})

export default store