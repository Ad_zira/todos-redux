import React from 'react'
import { useSelector } from 'react-redux'
import TodoListItem from './TodoListItem'
import { selectFilteredTodoIds } from './todosSlice'

const TodoList = () => {
  // const todoIds = useSelector(selectTodoIds, shallowEqual)
  const todoIds = useSelector(selectFilteredTodoIds)
  // console.log('This is todoID', todoIds)

  const loadingStatus = useSelector(state => state.todos.status)

  if (loadingStatus === 'loading') {
    return (
      <div className="todo-list">
        <div className="loader"/>
      </div>
    )
  }

  const renderedListItems = todoIds.map(todoId => {
    return <TodoListItem key={todoId} todo={todoId} />
  })

  return <ul className="todo-list">{renderedListItems}</ul>
}

export default TodoList