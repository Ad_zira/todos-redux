import React from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { ReactComponent as TimesSolid } from './times-solid.svg'
import { availableColors, capitalize } from '../filters/colors'
import {
  todoColorSelected,
  todoDeleted,
  todoToggled,
  selectTodoById,
} from './todosSlice'

// Destructure `props.id`, since we only need the ID value
const TodoListItem = (props) => {
  // console.log("id dari todoListItem", props)
  const id = props.todo
  // Call our `selectTodoById` with the state _and_ the ID value

  const todo = useSelector(state => selectTodoById(state, id))
  // const todo = useSelector(state => state.todos)
  const { text, completed, color } = todo

  const dispatch = useDispatch()

  const handleCompletedChanged = () => {
    dispatch(todoToggled(todo.id))
  }

  const handleColorChanged = e => {
    const color = e.target.value
    dispatch(todoColorSelected(todo.id, color))
  }
  const onDelete = () => {
    dispatch(todoDeleted(todo.id))
  }
  
  const colorOptions = availableColors.map(c => (
    <option key={c} value={c}>
      {capitalize(c)}
    </option>
  ))

  return (
    <li>
      <div className="view">
        <div className="segment label">
          <input 
            type="checkbox" 
            className="toggle" 
            checked={completed}
            onChange={handleCompletedChanged}
          />
          <div className="todo-text">{text}</div>
        </div>
        <div className="segment buttons">
          <select 
            value={color} 
            style={{ color }}
            onChange={handleColorChanged}
            className="colorPicker"
          >
            <option value=""></option>
            {colorOptions}
          </select>
          <button className="destroy" onClick={onDelete}>
            <TimesSolid />
          </button>
        </div>
      </div>
    </li>
  )
}

export default TodoListItem